### Readme - JS Plain - Closures - Basic

A brief demo of basic closure usage - simple demo to show global scope as persistent closure

  * VARS: declare variable in global scope
  * FN: basic outer function defined in global scope
    * parameters optional, output to console &c. for testing
  * Use: call function to test access to global scope closure
