/*
  js plain - functions - closures - basic
    - v. simple closure using global scope...
    - n.b. global scope is a closure - never goes away whilst app is running
*/

//value in global scope
var outerVal = "test1";

// function in global scope
function outerFn() {
//check & output result...
 console.log(outerVal === "test1" ? "test is visible..." : "test not visible...");
}

//execute function
outerFn();
