### Readme - JS Plain - Closures - Basic Inner

A brief demo of basic inner closure usage - nested function access to scoped variables.

  * VARS: required global scope variables, e.g. `outerVal` and `laterVal`
  * FN: outer function acting as parent function and container
    * inner function has access to outer function variables &c.
    * inner variables restricted to scope of inner function
    * outer function does not directly access inner function
  * Use:
    * outerFN() called
    * `laterVal` provides access to innerFN() in global scope
    * inner function as laterVal() called as function in global scope
