/*
  js closures - test 2
    - inner scope
*/

//variables in global scope
var outerVal = "test2";
var laterVal;

function outerFn() {
  //inner scope variable declared with value - scope limited to function
  var innerVal = "test2inner";
  //inner function - can access scope from parent function & variable innerVal
  function innerFn() {
    console.log(outerVal === "test2" ? "test2 is visible" : "test2 not visible");
    console.log(innerVal === "test2inner" ? "test2inner is visible" : "test2inner is not visible");
  }
  //inner function now added to global scope - now able to access elsewhere & call later
  laterVal = innerFn();
}
// invokes outerFn, innerFn is created, and its reference assigned to laterVal - outerFN does not log anything...
outerFn();
// THEN - innerFn is invoked using laterVal - can't access innerFn directly...
laterVal;
