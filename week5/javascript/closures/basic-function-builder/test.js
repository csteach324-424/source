/*
  js closures - basic function builder
    - build a function from another function using closures
*/

// define count function
function count(a) {
return function(b) {
      return a + b;
  }
}

// create new closure based functions...
var add1 = count(1);
var add5 = count(5);
var add10 = count(10);

// log output to console...
console.log(add1(8));
console.log(add5(8));
console.log(add10(8));
