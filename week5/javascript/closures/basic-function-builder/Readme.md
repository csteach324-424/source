### Readme - JS Plain - Closures - Basic Inner

A brief demo of basic function building with closures.

  * FN:
    * define function for building more functions
    * expects single parameter value
    * return new function with simple addition calculation
      * expects single parameter value
  * VARS:
    * add variables to build new functions
      * pass values for expected parameters
  * Use:
    * log output from new functions
